#!/bin/bash
# Script para Pós Instalação dos sistemas baseados em Ubuntu por Marcelo Rocha 18/07/2017

apt update && apt install dialog

dialog --msgbox "

           __________________________________\n
            / _______by__Gênios_Livres________ \ \n 
           / /                                \ \ \n
          | |                                  | |\n
          | |  Script pós instalação Ubuntu    | |\n
          | |                                  | |\n
          | |  O programa vai :                | |\n
          | |                                  | |\n
          | |  1 -  Atualizar os repositórios  | |\n
          | |  2 -  Instalar o Samba           | |\n
          | |  3 -  Instalar o Gimp e o VLC    | |\n
          | |  4 -  Instalar as fontes MS      | |\n
          | |  6 -  Instalar plugins e etc     | |\n
          | |  7 -  Instalar o Wine (Opc)      | |\n
          | |  8 -  Atualizar o sistema (Opc)  | |\n
          | |                                  | |\n
          \ \__________________________________/ /\n
           \____________________________________/\n" 25 65

	dialog --msgbox "Este programa vai fazer alterações na sua maquina!" 10 50
	dialog --msgbox "Aperte ENTER Digite 1 para continuar ou 2 para sair e aperte ENTER novamente " 10 50
	read eula
		if [ $eula = 1 ]; then
			echo ""
		elif [ $eula = 2 ]; then
			exit
		else
			dialog --msgbox "Opção Inválida" 10 50
			sh Ubuntu_post_install.sh
	fi;

	dialog --msgbox "Agora vamos instalar o samba ( responsavel por gerenciar e compartilhar arquivos em uma rede MS)!" 10 50
	apt install samba
	dialog --msgbox "Samba instalado com sucesso" 10 50
	dialog --msgbox "Agora vamos instalar o Gimp e o VLC (Programas para edição de imagens e reprodução de Videos de diversos formatos" 10 50
	apt install gimp vlc
	dialog --msgbox "Gimp e VLC instalados com sucesso!" 10 50
	dialog --msgbox "Agora vamos instalar o restante dos plugins!" 10 50
	apt install ubuntu-restricted-extras
	dialog --msgbox "Plugins  e fontes instaladas com sucesso!" 10 50
	dialog --msgbox "Voce deseja instalar o wine ? (Software capaz de rodar alguns programas do MS Windows no GNU/Linux)" 10 50
	dialog --msgbox "Aperte ENTER e digite 1 para SIM ou 2 para NÃO e aperte ENTER novamente" 10 50
	read wine
		if [ $wine  = 1 ]; then
			apt install wine-stable
			dialog --msgbox "Wine instalado com sucesso!" 10 50
		elif [ $wine = 2 ]; then
			dialog --msgbox "O Wine não será instalado !" 10 50
		else
                        dialog --msgbox "Opção Inválida" 10 50
			sh Ubuntu_post_install.sh 
	fi;

	dialog --msgbox "Você deseja dar um upgrade total no sistema ? (requer reinicialização)" 10 50
	dialog --msgbox "Aperte ENTER e digite 1 para SIM ou 2 para NÃO e aperte ENTER novamente" 10 50
	read upgrade
		if [ $upgrade = 1 ]; then
			apt dist-upgrade
		elif [ $upgrade = 2 ]; then
			echo ""
		else
                        dialog --msgbox "Opção Inválida" 10 50 
			sh Ubuntu_post_install.sh

	fi;

	dialog --msgbox "Obrigado por utilizar o Script pós instalação Ubuntu  by M. Rocha (Gênios Livres)" 10 50
	exit
